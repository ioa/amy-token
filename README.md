# amy-token 
AMYToken token and AMYCrowdsale contract for the AMY Crowdsale.
A social token for music makers to earn, protect their work and free themselves.
The AMY Token enables platform economy, Utility and Governance for all AMY platforms devoted to developing/emergent musicians worldwide.

https://token.amy.network
https://amy.network
https://studio.amy.network


500+ million people worldwide are developing musicians fueled by passion that drives billions in transactions of which musicians are the last beneficiaries. 
The music industry is not focused on them because the system is centralized, fragmented and dated.


AMY Music is a global social network for developing musicians built to recapture these transaction volumes.


The AMY Token is a social token for utility and governance to enable AMY platform economy, and accelerate the transition to a decentralized governance model, transforming users/musicians into stakeholders of this modern social network.


Get the Whitepaper on: https://token.amy.network

## deployments

# Mumbai

-------------------------------------------------------------------------------
deployed AMY Token address      : 0x984b13A9865Af52DD2d82Ff80720726b0b3ebFF5
deployed AMY Sale address       : 0xa43FC8914Ef296353EBCE41c34BC5751f65Ce739
deployed AMY Vesting            : 0xEd99DF74C17e8971cA266e355C0F143BbB609Be4
deployed AMY Airdrop            : 0x643707E13a4955Ee6b418F4B4f3E6d4FB87f3FF8
deployed USDC Test Token address: 0xf1dd34936Bc1C13aC312eF3dE88f559504D6262d
-------------------------------------------------------------------------------

# Polygon