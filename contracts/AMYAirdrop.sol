// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import './AMYToken.sol';
import '@openzeppelin/contracts/access/Ownable.sol';
import '@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol';
import '@openzeppelin/contracts/utils/math/SafeMath.sol';

/** 
  * @title AMYAirdrop airdrop contract. 
  * @author Steve Mariani
  * @notice This contract is for the airdop of AMY tokens to all existing AMY users that claim it. 
            There is only one airdrop, scheduled to coincide with AMY Listing time on a DEX,
            as defined in the Whitepaper https://token.amy.network/assets/docs/AMY_Token_WHITEPAPER.pdf.
  */
contract AMYAirdrop is Ownable {
  using SafeMath for uint256;
  using SafeERC20 for IERC20;
 
  AMYToken public amy;
  address public amyWallet;
  uint256 public amyAirdropTime;
  uint256 public airdropSupply;
  uint256 public distributedAmount = 0;
  uint256 public firstAridropShare;    

  // Keeps track of whether or not a AMY token airdrop has been made to a particular address
  mapping (address => bool) public airdrops;

  /** 
    * @notice Event to signal the successful airdrop delivery of a AMY tokens.. 
    * @param amount the amount of AMY token airdropped.
    */
  event TokensAirdropped(uint256 amount);


  constructor (
    AMYToken amyAddress, 
    uint256 airdropTime
    ) {
    amy = AMYToken(amyAddress);
    amyAirdropTime = airdropTime;
    airdropSupply = amy.balanceOf(address(this));
  }

  /** 
    * @notice retreives the amount of AMYs available on this airdrop contract
    * @return unit526 the amount of AMYs in this airdrop contract
    */
  function getAvailableAmount() public view returns(uint256){
    return amy.balanceOf(address(this));
  }

  /** 
    * @notice retreives the amount of AMYs that has been distributed
    * @return unit526 the amount of AMYs distributed
    */
  function getDistributedAmount() public view returns(uint256){
    return distributedAmount;
  }

  /** 
    * @notice retreived the amount of AMYs that have not been distributed
    * @return unit526 the amount of AMYs non distributed
    */
  function getUndistributedAmount() public view returns(uint256){
    return getAvailableAmount().sub(getDistributedAmount());
  }

  /** 
    * @notice the main function of this contract, to begin the airdrop
    * @param _recipient the array containing wallet addresses
                        this must be sent in at 100 addresses at a time, not 40000 altogether in one lump!!!
    */
  function airdropTokens(address[] memory _recipient) public onlyOwner {
    uint totalRecipients = _recipient.length;
    
    require(block.timestamp >= amyAirdropTime, 'AMY Airdrop: airdroptime is in the past.');
    require(totalRecipients > 0, 'AMY Airdrop: there are no recipients for the airdrop.');
   
    firstAridropShare = getAvailableAmount().div(totalRecipients);

    for(uint256 i = 0; i< totalRecipients; i++){
        if (!airdrops[_recipient[i]]) {
          airdrops[_recipient[i]] = true;
          require(amy.transfer(_recipient[i], firstAridropShare ));
          distributedAmount = distributedAmount.add( firstAridropShare );
        }
    }
    airdropSupply = airdropSupply.sub(distributedAmount);

    emit TokensAirdropped(distributedAmount);
  }
 

  /** 
    * @notice called to set the timestamp for the beginning of the airdrop
    * @param t the timespemp to start the airdrop    
    */
  function setAirdropTime(uint256 t) public onlyOwner {
    amyAirdropTime = t;
  }

 
  /** 
    * @notice once the airdrop is done this method can be called to relinquish left over balances
              and destroy the contract.
    */
  function finalize() public onlyOwner {
    uint256 balance = amy.balanceOf(address(this));
    require(amy.transfer(msg.sender, balance), 'AMY Airdrop: finilizing transfer issue.');
    selfdestruct(payable(msg.sender));
  }
}