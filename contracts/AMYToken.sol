// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol';
import '@openzeppelin/contracts/access/Ownable.sol';

/** 
  * @title AMYToken token contract for a standards ERC20 token. 
  * @author Steve Mariani & Ramon Canales
  * @notice This contract defineds the AMY token as described
            in the Whitepaper https://token.amy.network/assets/docs/AMY_Token_WHITEPAPER.pdf.. 
            The AMY Token is a standard ERC20 token
            with initially minted supply at genesis of 3b as per Whitepaper.
  */

contract AMYToken is ERC20PresetMinterPauser, Ownable {

  string private _name = 'AMY Token';
  string private _symbol = 'AMY';
  uint256 private _initialSupply = 3000000000 * (10 ** uint256(18));

  /** 
    * @notice All AMY Tokens are minted at launch and deposited in owner's wallet.
    */
  constructor() ERC20PresetMinterPauser(_name, _symbol) {
    mint(msg.sender, _initialSupply);
  }
 
}