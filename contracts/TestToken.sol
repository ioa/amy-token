// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol';
import '@openzeppelin/contracts/access/Ownable.sol';

/** 
  * @title TestToken ERC20 token contract purely for teststing. 
  * @author Steve Mariani
  * @notice This contract is exclusively for localnet and testnet testing to 
            simulate another ERC20 token that is being used to purchase AMY Tokens.
            This contract is not do be deployed on Mainnet. No need.
  */
contract TestToken is ERC20PresetMinterPauser, Ownable {

  string public _name;
  string public _symbol;
  constructor(   
      string memory name,
      string memory symbol
  ) ERC20PresetMinterPauser(name, symbol) {      
    mint(msg.sender, 950000 * (10 ** uint256(decimals())) );
  }
 
  function distribute(address[] memory wallets) public onlyOwner {
    for (uint i=0; i<wallets.length; i++) {      
      mint(wallets[i], 215000 * (10 ** uint256(decimals()) ) );  
    }
  }

 function decimals() public view virtual override returns (uint8) {
    return 6;
  }

  function destroy() public onlyOwner {
      selfdestruct(payable(msg.sender));
  }

}
