// SPDX-License-Identifier: MIT
const { default: BigNumber } = require("bignumber.js");
const TestToken = artifacts.require("TestToken");
const AMYToken = artifacts.require("AMYToken");
const AMYSale = artifacts.require("AMYSale");
const AMYVesting = artifacts.require("AMYVesting")
const AMYAirdrop = artifacts.require("AMYAirdrop");
const decimals = 10**18;
const totalSupply = 3000000000*decimals;

let listingTime = new Date().getTime() / 1000 | 0;
let airdropTime = new Date().getTime() / 1000 | 0;
let vestingCliff = 0;           // inseconds form "now"   0 = 0 minutes from investment
let vestingDuration = 31536000; // inseconds form cliff date ("now" + cliff)   31536000 = 365 days from cliff
let usdc;
let tokenWallet;
let saleWallet;

const saleSupply =  new BigNumber(totalSupply * 0.1); // 10%  
const airdropSupply =  new BigNumber(totalSupply * 0.05); // 5% 

const oneDay = 86400;

module.exports = async function (deployer, network) {
      
  if (network == "development") {
    console.log("---- deploying in LOCAL DEVELOPMENT -----");

    vestingCliff = 0;  
    vestingDuration = 120;
    listingTime = (listingTime + 80);// 259200 = 3 days
    airdropTime = (airdropTime + 359200);// 

    // The following are local development addresses from your local ganache setup, subject to change.
    tokenWallet=  "0x71647Ab44704077583BCEcD42575b9b2a7607872"; // 1st wallet address from ganache.
    saleWallet =  "0xC6D98b10238739d472cf185976212d8801ce2380"; // 2ND sale address
    team_Ramon =  "0x460e75D7382575F2C4ADCd13e88a962Ec916B654"; // 4th Ramon   
    team_Nadia =  "0x37dD3c5fD1021edd4D3c7f8227463A762eB986b3"; // 5TH Nadia
    team_Steve =  "0x72860F88C06e1A3674f10178a2f7E644286502b5"; // 8th Steve
    team_Ric   =  "0x5E31eB4A62d925559BeB33f3bD43E85c4eC42689"; // 7th Ric

    inv_1 =       "0x72860F88C06e1A3674f10178a2f7E644286502b5"; // 8th "8-P.2/INV B"
    inv_2 =       "0x460e75D7382575F2C4ADCd13e88a962Ec916B654"; // 4th INV B/Ramon
    inv_3 =       "0x37dD3c5fD1021edd4D3c7f8227463A762eB986b3"; // 5th INV C/Nadia
    inv_4 =       "0x37dD3c5fD1021edd4D3c7f8227463A762eB986b3"; // 6th INV D/Ric

  } else if (network == "mumbai" || network == "testnet") { 

    console.log("---- deploying in MUMBAI TESTNET 1 -----");
    
    vestingCliff = 0;     // inseconds form "now"   300 = 5 minutes from investment
    vestingDuration = oneDay*6; 
    listingTime = (listingTime + oneDay*4); 
    airdropTime = (airdropTime + 272800);

    console.log("---- deploying in MUMBAI TESTNET 2 -----");

    // DO NOT EDIT ANY of the following addresses for Testnet
    tokenWallet=  "0x71647Ab44704077583BCEcD42575b9b2a7607872"; // Steve account. "AMY Wallet"
    saleWallet =  "0xC6D98b10238739d472cf185976212d8801ce2380"; // Steve account. "SALE Wallet"
    team_Steve =  "0x457f2291324852f3963f0A99cF88A51C42A87994"; // Steve account 1. "3-P.1/INV A"
    team_Ramon =  "0x91455BACbDB3bD379783272ee3fc9841F5c7aC39"; // Ramon account 1.
    team_Nadia =  "0xB6F46A4597A3Fd515F6cc46Ab2dfFcE8036CD4f4"; // Nadia account 1.    
    team_Ric   =  "0x9610E438c473093e0A75Ce7bb3e092400585E8b0"; // Riccardo account 1.

    //these are used for simulations in testnet (posing as investor, advisor, or user)
    inv_1 =       "0x72860F88C06e1A3674f10178a2f7E644286502b5"; // Steve account 2. "8-P.2/INV B"
    inv_2 =       "0xe5b06bfd663C94005B8b159Cd320Fd7976549f9b"; // Ramon account 2.
    inv_3 =       "0x3fE252Cbf3d34aAC6aEF4ee485890D672B836469"; // Nadia account 2.
    inv_4 =       "0x71aaC5C4C357131c39De14DDA219dFA0357197Df"; // Riccardo account 2.
                   
    console.log("---- deploying in MUMBAI TESTNET 3 -----");

  } else if (network == "matic" || network == "mainnet") { // LIVE NETWORK
    
    console.log("********** LIVE DEPLOYMENT IN MATIC MAINNET *********");
    
    vestingCliff = 0;       // inseconds form "now"   0 = 0 minutes from investment
    vestingDuration = 31536000;  // inseconds form cliff date ("now" + cliff)   31536000 = 365 days from cliff
    listingTime = (listingTime + 5184000); // 5184000 = 60 days
    airdropTime = (airdropTime + 5284000);// 

    
    // DO NOT EDIT ANY of the following addresses for Mainnet
    usdc= '0x2791bca1f2de4661ed88a30c99a7a9449aa84174';

    tokenWallet=  "0x71647Ab44704077583BCEcD42575b9b2a7607872"; // Steve account. "AMY Wallet"
    saleWallet =  "0xC6D98b10238739d472cf185976212d8801ce2380"; // Steve account. "SALE Wallet"
    team_Steve =  "0x457f2291324852f3963f0A99cF88A51C42A87994"; // Steve account 1. "3.Steve Personal Account 1/A"
    team_Ramon =  "0x91455BACbDB3bD379783272ee3fc9841F5c7aC39"; // Ramon account 1.
    team_Nadia =  "0xB6F46A4597A3Fd515F6cc46Ab2dfFcE8036CD4f4"; // Nadia account 1.    
    team_Ric   =  "0x9610E438c473093e0A75Ce7bb3e092400585E8b0"; // Riccardo account 1.

  }


  // USDC Test Token deployment on Localnet or Testnet only
  if ( (network == "development") ||  (network == "mumbai") ||  (network == "testnet")) {    
    console.log("---- deploying in MUMBAI TESTNET 4 -----");

    await deployer.deploy(TestToken,
      "USDC Test Token v3.2", 
      "UDCSv3.2"    
    );
    testToken = await TestToken.deployed();    
    console.log("deployed USDC Test Token address: "+ testToken.address);
   
    const wallets = [team_Ramon, team_Nadia, team_Ric, team_Steve, inv_1, inv_2, inv_3, inv_4];
    await testToken.distribute(wallets);

    console.log("---- deploying in MUMBAI TESTNET 5 Trsnferred USDC -----");  
    
    usdc= testToken.address;
    
  }


  // AMYToken deployment
  await deployer.deploy(AMYToken);
  const amy = await AMYToken.deployed();
  console.log("---------------------> deployed AMY Token address: "+ amy.address);

  
  //AMY Vesting deployment  
  await deployer.deploy(AMYVesting, 
    amy.address, 
    saleWallet,
    listingTime,
    vestingDuration
  );
  
  const vesting = await AMYVesting.deployed();  
  console.log("---------------------> deployed AMY Vesting: "+ vesting.address);


  // AMYSale deployment
  await deployer.deploy(
    AMYSale,
    amy.address,
    vesting.address,
    saleWallet,
    usdc
  );
  const sale = await AMYSale.deployed();   
  console.log("---------------------> deployed AMY Sale address: "+ sale.address);


  // AMYAirdropSale deployment
  await deployer.deploy(
    AMYAirdrop,    
    amy.address,
    airdropTime
  );
  const airdrop = await AMYAirdrop.deployed();   
  console.log("---------------------> deployed AMY Sale address: "+ sale.address);
  

  ////////////////////////////////////////////
  //This is required since Sale calls vesting.deposit() and it is an onlyAdmin method for security restriction.
  await vesting.setDepositer(sale.address);
  ////////////////////////////////////////////

  ////////////////////////////////////////////
  // fund the Sale contract
  await amy.transfer(sale.address, saleSupply);
  ////////////////////////////////////////////
  
  ////////////////////////////////////////////
  // fund the Airdrop contract
  await amy.transfer(airdrop.address, airdropSupply);
  ////////////////////////////////////////////

  

  console.log("                                                                               ");
  console.log("----------IMPORTANT: Copy & paste these address into Readme--------------");
  console.log("-------------------------------------------------------------------------------");
  console.log("deployed AMY Token address      : "+ amy.address);
  console.log("deployed AMY Sale address       : "+ sale.address);  
  console.log("deployed AMY Vesting            : "+ vesting.address);
  console.log("deployed AMY Airdrop            : "+ airdrop.address);
  console.log("deployed USDC Test Token address: "+ testToken.address);
  console.log("-------------------------------------------------------------------------------");

  // console.log("var amyLocalnet='"+ amy.address +"';");
  // console.log("var saleLocalnet='"+ sale.address +"';");
  // console.log("var vestingLocalnet='"+ vesting.address +"';");
  // console.log("var usdcLocalnet='"+ testToken.address +"';");
  

};
