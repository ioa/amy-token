const { accounts, contract } = require('@openzeppelin/test-environment');
const { BN, constants, expectEvent, expectRevert, time } = require('@openzeppelin/test-helpers');
const [ admin, purchaser, user ] = accounts;
const { ZERO_ADDRESS } = constants;
const { expect } = require('chai');

const Web3 = require('web3');
const Token = contract.fromArtifact('AMYToken');
const Vesting = contract.fromArtifact('AMYVesting');
const Sale = contract.fromArtifact('AMYSale');



describe('AMYSale', function () {
  /*
  const [ deployer, owner, wallet, investor ] = accounts;

  const saleRate = new BN(1, 10);
  const saleCap = ether('10000');//new BN('15000000',10);
  const investmentAmount = ether('4');
  const expectedTokenAmount = saleRate.mul(investmentAmount);

  before(async function () {
    // Advance to the next block to correctly read time in the solidity "now" function interpreted by ganache
    await time.advanceBlock();
  });

  beforeEach(async function () {
    //this.openingTime = (await time.latest()).add(time.duration.weeks(1));
    //this.closingTime = this.openingTime.add(time.duration.weeks(1));
    //this.afterClosingTime = this.closingTime.add(time.duration.seconds(1));

    this.token = await AMYToken.new({ from: deployer });

    this.crowdsale = await Sale.new( (await this.token.address), saleRate, wallet, saleCap, { from: deployer });
    
    this.token.transfer((await this.crowdsale.address), saleCap, { from: deployer });


    
  });
 
  it("should return basic hardcoded data", async ()=> {
    console.log("______________________________________________________");
    console.log(accounts);
    console.log("deployer     : "+ deployer);
    console.log("owner        : "+ owner);
    console.log("wallet       : "+ wallet);
    console.log("investor     : "+ investor);
    console.log("this.address : "+ (await this.address));
    console.log("RATE         : "+ saleRate);
    console.log("CAP          : "+ saleCap);
    console.log("______________________________________________________");
  });

  it("should return correct AMY Token data", async function () {
    console.log("address           : "+ await this.token.address );
    console.log("name              : "+ await this.token.name() );
    console.log("symbol            : "+ await this.token.symbol() );
    console.log("decimalsl         : "+ await this.token.decimals() );
    console.log("token cap         : "+ (await this.token.cap()).toString() );
    console.log("token totalSupply : "+ (await this.token.totalSupply()).toString() );
    console.log("deployer balanceOf: "+ (await this.token.balanceOf(deployer)).toString() );
    console.log("sale cap          : "+ saleCap );
  });

  it("should return correct AMY Sale data ", async function () {
    let cap = (await this.crowdsale.cap()).toString();
    let bal = (await this.token.balanceOf(await this.crowdsale.address)).toString();
    let wal = (await this.token.balanceOf(await this.crowdsale.wallet())).toString();
    console.log( "sale address     : "+ await this.crowdsale.address );
    console.log( "sale adr balance : "+ bal );
    console.log( "sale cap         : "+ cap );
    console.log( "sale rate        : "+ await this.crowdsale.rate() );
    console.log( "sale weiRaised   : "+ await this.crowdsale.weiRaised() );
    console.log( "sale wallet adr  : "+ await this.crowdsale.wallet() );
    console.log( "sale wal balance : "+ wal );
    expect(await this.crowdsale.rate()).to.be.bignumber.equal(saleRate);
    expect(await this.crowdsale.wallet()).to.equal(wallet);    
    expect( cap ).to.be.bignumber.equal( saleCap );   
  });

  it("AMY Sale should accept payments .send() ", async function () {
    console.log("_BEFORE____________________________________");
    console.log("deployer AMY    : "+ (await this.token.balanceOf(deployer)).toString() );
    console.log("deployer  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("investor AMY    : "+ (await this.token.balanceOf(investor)).toString() );
    console.log("investor  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("wallet   AMY    : "+ (await this.token.balanceOf(wallet)).toString() );
    console.log("wallet    ETH   > "+ await web3.eth.getBalance(wallet) );
    console.log("sale     AMY    : "+ (await this.token.balanceOf(await this.crowdsale.address)).toString() );
    console.log("sale      ETH   > "+ await web3.eth.getBalance(await this.crowdsale.address) );
    console.log("sale     CAP    : "+ (await this.crowdsale.cap()).toString() ); 

    await this.crowdsale.send( investmentAmount, {from: investor } );    

    console.log("_AFTER____________________________________");
    console.log("deployer AMY    : "+ (await this.token.balanceOf(deployer)).toString() );
    console.log("deployer  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("investor AMY    : "+ (await this.token.balanceOf(investor)).toString() );
    console.log("investor  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("wallet   AMY    : "+ (await this.token.balanceOf(wallet)).toString() );
    console.log("wallet    ETH   > "+ await web3.eth.getBalance(wallet) );
    console.log("sale     AMY    : "+ (await this.token.balanceOf(await this.crowdsale.address)).toString() );
    console.log("sale      ETH   > "+ await web3.eth.getBalance(await this.crowdsale.address) );
    console.log("sale     CAP    : "+ (await this.crowdsale.cap()).toString() ); 

    await expect(await this.token.balanceOf(investor)).to.be.bignumber.equal(expectedTokenAmount);
    //await expect(await this.token.balanceOf(wallet)).to.be.bignumber.equal(expectedTokenAmount);
  });

  it('should accept payments during the sale', async function () {
    console.log("_BEFORE____________________________________");
    console.log("deployer AMY    : "+ (await this.token.balanceOf(deployer)).toString() );
    console.log("deployer  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("investor AMY    : "+ (await this.token.balanceOf(investor)).toString() );
    console.log("investor  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("wallet   AMY    : "+ (await this.token.balanceOf(wallet)).toString() );
    console.log("wallet    ETH   > "+ await web3.eth.getBalance(wallet) );
    console.log("sale     AMY    : "+ (await this.token.balanceOf(await this.crowdsale.address)).toString() );
    console.log("sale      ETH   > "+ await web3.eth.getBalance(await this.crowdsale.address) );
    console.log("sale     CAP    : "+ (await this.crowdsale.cap()).toString() ); 

    const balanceTracker = await balance.tracker(wallet);
    //await time.increaseTo(this.openingTime);
    await this.crowdsale.buyTokens(investor, {  from: investor, value: investmentAmount });
    //await wallet.transfer({ value: investmentAmount, from: investor });

    console.log("_AFTER____________________________________");
    console.log("deployer AMY    : "+ (await this.token.balanceOf(deployer)).toString() );
    console.log("deployer  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("investor AMY    : "+ (await this.token.balanceOf(investor)).toString() );
    console.log("investor  ETH   > "+ await web3.eth.getBalance(investor) );
    console.log("wallet   AMY    : "+ (await this.token.balanceOf(wallet)).toString() );
    console.log("wallet    ETH   > "+ await web3.eth.getBalance(wallet) );
    console.log("sale     AMY    : "+ (await this.token.balanceOf(await this.crowdsale.address)).toString() );
    console.log("sale      ETH   > "+ await web3.eth.getBalance(await this.crowdsale.address) );
    console.log("sale     CAP    : "+ (await this.crowdsale.cap()).toString() ); 
    
    expect(await this.token.balanceOf(investor)).to.be.bignumber.equal(expectedTokenAmount);
    expect(await balanceTracker.delta()).to.be.bignumber.equal(investmentAmount);
    //await expect(await this.token.balanceOf(wallet)).to.be.bignumber.equal(expectedTokenAmount);
  });

  it('should reject payments over cap', async function () {
    //await time.increaseTo(this.openingTime);
    await this.crowdsale.buyTokens(investor, { value: saleCap, from: investor });
    await expectRevert(await this.crowdsale.send(1), 'AMYCrowdsale: cap exceeded');
  });
*/
});