const { accounts, contract } = require('@openzeppelin/test-environment');
const { BN, constants, expectEvent, expectRevert, time } = require('@openzeppelin/test-helpers');
const [ admin, purchaser, user ] = accounts;
const { ZERO_ADDRESS } = constants;
const { expect } = require('chai');

const Web3 = require('web3');
const Token = contract.fromArtifact('AMYToken');
const Vesting = contract.fromArtifact('AMYVesting');
const Sale = contract.fromArtifact('AMYSale');

describe('AMYToken', function () {
  const [ creator ] = accounts;

  beforeEach(async function () {
    this.token = await AMYToken.new({ from: creator });
  });

  it('has a name', async function () {
    expect(await this.token.name()).to.equal('AMY Token');
  });

  it('has a symbol', async function () {
    expect(await this.token.symbol()).to.equal('AMY');
  });

  it('has 18 decimals', async function () {
    expect(await this.token.decimals()).to.be.bignumber.equal('18');
  });

  it('assigns the initial total supply to the creator', async function () {
    const totalSupply = await this.token.totalSupply();
    const creatorBalance = await this.token.balanceOf(creator);

    expect(creatorBalance).to.be.bignumber.equal(totalSupply);

    await expectEvent.inConstruction(this.token, 'Transfer', {
      from: ZERO_ADDRESS,
      to: creator,
      value: totalSupply,
    });
  });
});
